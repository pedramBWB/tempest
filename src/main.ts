/* eslint-disable */
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import VCalendar from "v-calendar";

// import Icon Font Awesome
import { faBars } from "@fortawesome/free-solid-svg-icons";

// Utilisation des icons niveau globale
library.add(faBars);

// Composant Font Awesome
Vue.component("font-awesome-icon", FontAwesomeIcon);

// BootstrapVue
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

// Utilisation des imports
Vue.use(BootstrapVue).use(IconsPlugin);

//Utilisation de Vue Calendar
Vue.use(VCalendar, {});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
