import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    plant: {
      id: "",
      name: "",
      minHeat: "",
      maxHeat: "",
      windRES: "",
      waterFrequence: "",
      waterVol: "",
      plantationDate: "",
      harvestDate: "",
    },
    plantList: [
      {
        id: "1",
        name: "",
        minHeat: "",
        maxHeat: "",
        windRES: "",
        waterFrequence: "",
        waterVol: "",
        plantationDate: "",
        harvestDate: "",
      },
    ],
    dd: 0,
    fd: 0,
    tt: "",
    ct: "",
  },
  mutations: {
    addPlant(state, plantSub) {
      console.log(plantSub);

      plantSub.id = "2";
      plantSub.name = state.plant.name;
      plantSub.minHeat = state.plant.minHeat;
      plantSub.maxHeat = state.plant.maxHeat;
      plantSub.windRES = state.plant.windRES;
      plantSub.waterFrequence = state.plant.waterFrequence;
      plantSub.waterVol = state.plant.waterVol;
      plantSub.plantationDate = state.plant.plantationDate;
      plantSub.harvestDate = state.plant.harvestDate;

      state.plantList.push(state.plant);

      console.log(state.plantList);
    },
    titreTodo(state, data) {
      state.tt = data;
    },
    contenuTodo(state, data) {
      state.ct = data;
    },
    debutDate(state, data) {
      state.dd = data;
    },
    finDate(state, data) {
      state.fd = data;
    },
  },
  getters: {
    titreTodoGetter(state) {
      return state.tt;
    },
    contenuTodoGetter(state) {
      return state.ct;
    },
    debutDateGetter(state) {
      return state.dd;
    },
    finDateGetter(state) {
      return state.fd;
    },
  },
  actions: {},
  modules: {},
});
